import { Component } from '@angular/core';
import { UserService } from './servicios/user.service'
import { ProductoService } from 'app/servicios/producto.service';

import '../styles/bootstrap-4.4.1-dist/css/bootstrap.min.css';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService,ProductoService]
})
export class AppComponent {
  title = 'app works!';
}
