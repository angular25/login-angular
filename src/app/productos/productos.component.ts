import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'app/servicios/producto.service';
import { BarraNavegacionComponent } from '../barra-navegacion/barra-navegacion.component'
import { Router } from '@angular/router';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
  private lista = [];  
  private auxLista = [];

  constructor(private productoService: ProductoService, private router: Router) {
    this.lista = this.productoService.getProductos()
  }

  agregar (item,cantidad){
    if(cantidad.value && cantidad.value <= item['cantidad']){
      this.productoService.AgregarProducto(item,cantidad);
    }
  }

  buscar (search){
    this.lista = this.productoService.getProductos();

    if ( search.trim().length > 0 ){

      this.auxLista = this.lista.filter((product) => product.nombre.includes(search))
      this.lista = this.auxLista;

    }
  }

  returnDetalle(item){
    this.productoService.AgregarDetalle(item);
    this.router.navigate(['/detalle']);
    return true;
  }

}
