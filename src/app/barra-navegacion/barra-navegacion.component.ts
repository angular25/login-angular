import { Component } from '@angular/core';
import { ProductoService } from 'app/servicios/producto.service';
import { Router } from '@angular/router';
import { UserService } from 'app/servicios/user.service'

@Component({
  selector: 'barra-navegacion',
  templateUrl: './barra-navegacion.component.html',
  styleUrls: ['./barra-navegacion.component.css']
})
export class BarraNavegacionComponent  {
  private lista = [];
  private isActive = false;

  constructor(private productoService: ProductoService, private userService: UserService, private router: Router) { 
    this.lista = this.productoService.getTotal();
    this.isActive = this.userService.IsActive();
  }

  detalleCarrito(){
    this.router.navigate(['/carrito']);
    return true;
  }

  salir(){
    this.userService.logout();
    this.router.navigate(['']);
    return true;
  }

}
 