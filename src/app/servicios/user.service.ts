import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class UserService {

  constructor (private router: Router) {}

  // arreglo de usuarios 
  private usuario = [
    {
        "email": "demo@gmail.com",
        "pass": "secret",
        "isActive": false
    }
  ];

  getUsers(nombre: string, pass: string){
    console.log(this.usuario);
    if(this.usuario[0]["email"] == nombre && this.usuario[0]["pass"] == pass){
      this.usuario[0]["isActive"] = true;
      console.log(this.usuario);
      return true;
    }
    return false;
  }

  IsActive(){
    console.log(this.usuario);
    if (!this.usuario[0]["isActive"]) {
      this.router.navigate(['/']);
      return true;
    }
  }

  logout(){
    this.usuario[0]["isActive"] = false;
  }

}
