/*import { Injectable } from '@angular/core';

@Injectable()*/
export class ProductoService {
  // arreglo de usuarios 
  private productos = [
    { "nombre": "Aguacate", "precio": 5, "cantidad": 35, "img": 'aguacate.jpg' },
    { "nombre": "Ajo", "precio": 2, "cantidad": 35, "img": 'ajo.jpg' },
    { "nombre": "Almendras", "precio": 4, "cantidad": 35, "img": 'almendras.jpg' },
    { "nombre": "Arandanos", "precio": 5, "cantidad": 35, "img": 'arandanos.jpg' },
    { "nombre": "brocoli", "precio": 5, "cantidad": 35, "img": 'brocoli.png' },
    { "nombre": "calabaza", "precio": 15, "cantidad": 35, "img": 'calabaza.jpg' },
    { "nombre": "canela", "precio": 5, "cantidad": 21, "img": 'canela.jpg' },
    { "nombre": "cebolla", "precio": 5, "cantidad": 35, "img": 'cebolla.jpg' },
    { "nombre": "fresa", "precio": 7, "cantidad": 35, "img": 'fresa.jpg' },
    { "nombre": "kiwi", "precio": 5, "cantidad": 45, "img": 'kiwi.jpg' },
    { "nombre": "limon", "precio": 3, "cantidad": 5, "img": 'limon.jpg' },
    { "nombre": "lychee", "precio": 5, "cantidad": 35, "img": 'lychee.jpg' },
    { "nombre": "maiz", "precio": 5, "cantidad": 15, "img": 'maiz.jpg' },
    { "nombre": "manzana", "precio": 10, "cantidad": 35, "img": 'manzana.jpg' },
    { "nombre": "naranja", "precio": 5, "cantidad": 35, "img": 'naranja.jpg' },
    { "nombre": "papa", "precio": 5, "cantidad": 35, "img": 'papa.jpg' },
    { "nombre": "pasta", "precio": 5, "cantidad": 31, "img": 'pasta.jpg' },
    { "nombre": "pimienta", "precio": 5, "cantidad": 25, "img": 'pimienta.jpg' },
    { "nombre": "repollo", "precio": 3, "cantidad": 35, "img": 'repollo.jpg' },
    { "nombre": "tomate", "precio": 2, "cantidad": 30, "img": 'tomate.jpg' },
    { "nombre": "zanahoria", "precio": 6, "cantidad": 35, "img": 'zanahoria.jpg' }
  ];

  private total = [0];
  private carrito = [];
  private detalle = [];

  getProductos(){
    return this.productos;
  }

  getCarrito() {
    return this.carrito;
  }

  getDetalle(){
    return this.detalle;
  }

  pagar () {
    this.carrito = [];
    this.total = [0];
  }

  AgregarProducto (item, cantidad){
    this.carrito.push({ 'producto': item,'cantidad': cantidad.value });
    //this.carrito.push(cantidad.value);
    // console.log(item['nombre']);
    this.productos.find(v => v.nombre == item['nombre']).cantidad = item['cantidad'] - cantidad.value;

    this.total.splice(0,1)
    this.total.push(this.carrito.length);
  }

  AgregarDetalle (item){
    this.detalle.splice(0,1)
    this.detalle.push(item);
    //console.log(this.detalle);
  }

  getTotal() {
    return this.total
  }

}
