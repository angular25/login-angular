import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'app/servicios/producto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent  {

  private detalle = [];

  constructor(private productoService: ProductoService, private router: Router) {
    this.detalle = this.productoService.getDetalle()
  }

  returnProducto(){
    this.router.navigate(['/productos']);
    return true;
  }

}
