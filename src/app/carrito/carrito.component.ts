import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'app/servicios/producto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent {
  private listProd = [];

  constructor(private productService: ProductoService, private router: Router) { 
    this.listProd = this.productService.getCarrito();
    //console.log(this.listProd);
  }

  returnProducto(){
    this.router.navigate(['/productos']);
    return true;
  }

  getTotal(){
    var total = 0;
    for (let index = 0; index < this.listProd.length; index++) {
      const element = this.listProd[index];
      total += (element.producto['precio'] * element.cantidad);
      
    }
    return total;
  }

  pagar(){
    this.productService.pagar();
    this.router.navigate(['/productos']);
    return true;
  }

}
