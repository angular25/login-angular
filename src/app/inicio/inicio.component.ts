import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { UserService } from 'app/servicios/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  miFormulario: FormGroup;

  constructor (private userService: UserService, private router: Router){}

  ngOnInit () {
    this.miFormulario = new FormGroup ({
      'email-grupo': new FormControl('demo@gmail.com', Validators.required),
      'pass-grupo': new FormControl('', Validators.required)
    })
  }

  enviarFormulario () {
    console.log(this.miFormulario.value['email-grupo'],this.miFormulario.value['pass-grupo']);
    if(this.userService.getUsers(this.miFormulario.value['email-grupo'], this.miFormulario.value['pass-grupo'])){
      this.router.navigate(['/productos']);
      return true;
    }
    this.router.navigate(['/']);
    return false
  }

}
