import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { InicioComponent } from '../inicio/inicio.component'
import { ProductosComponent } from '../productos/productos.component'
import { DetalleComponent } from '../detalle/detalle.component'
import { CarritoComponent } from '../carrito/carrito.component'

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'detalle', component: DetalleComponent },
  { path: 'carrito', component: CarritoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
